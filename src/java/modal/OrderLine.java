/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modal;

/**
 *
 * @author anhnb
 */
public class OrderLine {
    Order order;
    Product product;
    String prName;
    int quantity;
    float price;
    float amount;
    int sizeChoose;
    public OrderLine() {
    }
    
    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getPrName() {
        return prName;
    }

    public void setPrName(String prName) {
        this.prName = prName;
    }

  

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getAmount() {
        return amount=this.quantity*price;
    }

    public int getSizeChoose() {
        return sizeChoose;
    }

    public void setSizeChoose(int sizeChoose) {
        this.sizeChoose = sizeChoose;
    }

    @Override
    public String toString() {
        return "OrderLine{" + "prName=" + prName + ", quantity=" + quantity + ", price=" + price + ", amount=" + amount + ", sizeChoose=" + sizeChoose + '}';
    }

   
    
}
