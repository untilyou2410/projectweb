/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modal;

/**
 *
 * @author anhnb
 */
public class Product {
    int id;
    String name;
    int cateId;
    int M;
    int XL;
    int XXL;
    float price;
    String description;
    String imgPath;
   int active;
    public Product() {
    }

    public Product( String name, int cateId, int M, int XL, int XXL, float price,String description,String imgPath) {
      this.imgPath = imgPath;
        this.name = name;
        this.cateId = cateId;
        this.M = M;
        this.XL = XL;
        this.XXL = XXL;
        this.price = price;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCateId() {
        return cateId;
    }

    public void setCateId(int cateId) {
        this.cateId = cateId;
    }

    public int getM() {
        return M;
    }

    public void setM(int M) {
        this.M = M;
    }

    public int getXL() {
        return XL;
    }

    public void setXL(int XL) {
        this.XL = XL;
    }

    public int getXXL() {
        return XXL;
    }

    public void setXXL(int XXL) {
        this.XXL = XXL;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
     public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }
    
    
    @Override
    public String toString() {
        return id+ " "+name+" "+ imgPath;
    }
    
    
}
