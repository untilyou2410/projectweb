/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modal;

import java.sql.Date;
import java.util.ArrayList;

/**
 *
 * @author anhnb
 */
public class Order {
    private int ID;
    int cusID;
    int staffID;
    int shipID;
    Date datee;
    ArrayList<OrderLine> lines = new ArrayList<>();
    float total;
    public Order() {
    }
    
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getCusID() {
        return cusID;
    }

    public void setCusID(int cusID) {
        this.cusID = cusID;
    }

    public int getStaffID() {
        return staffID;
    }

    public void setStaffID(int staffID) {
        this.staffID = staffID;
    }

    public int getShipID() {
        return shipID;
    }

    public void setShipID(int shipID) {
        this.shipID = shipID;
    }

    public Date getDatee() {
        return datee;
    }

    public void setDatee(Date datee) {
        this.datee = datee;
    }

    public ArrayList<OrderLine> getLines() {
        return lines;
    }

    public void setLines(ArrayList<OrderLine> lines) {
        this.lines = lines;
    }

    public float getTotal() {
        for (OrderLine line : lines) {
            total+= line.getAmount();
        }
        return total;
    }
    public double getAmount(){
        double amount = 0;
        for (OrderLine line : lines) {
            amount+=line.getAmount();
        }
        return amount;
    }
    
}
