/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.auth;

import dal.categoryDAO;
import dal.productDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modal.Customer;
import modal.Order;
import modal.OrderLine;
import modal.Product;

/**
 *
 * @author anhnb
 */
public class shoppingCartController extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         response.setContentType("text/html;charset=UTF-8");
         request.setCharacterEncoding("UTF-8");
        categoryDAO cateDAO = new categoryDAO();
        Customer c = null;
        c = (Customer)request.getSession().getAttribute("customer");
        if (c == null){
            c = new Customer();
        }
        request.setAttribute("customer",c );
        response.sendRedirect("cart.jsp");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         response.setContentType("text/html;charset=UTF-8");
         request.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession();
        Order order = (Order)session.getAttribute("cart");
        categoryDAO cateDAO = new categoryDAO();
        int sizeChoose;
        if(order ==null)
            order=new Order();
         int productID = Integer.parseInt(request.getParameter("productID"));
         int cateID = Integer.parseInt(request.getParameter("cateID"));
         int quantity = Integer.parseInt(request.getParameter("quantity"));
         String sizeChooseS = (request.getParameter("inlineRadioOptions"));
         if(sizeChooseS == null)  sizeChoose=0;
         else sizeChoose=Integer.parseInt(sizeChooseS);
        productDAO db = new productDAO();
        Product product = db.getProductbyID(productID+"", cateID+"");
        boolean isExisting = false;
        for (OrderLine line : order.getLines()) {
            if(line.getProduct().getId() == product.getId())
            {   
                line.setQuantity( line.getQuantity() + quantity);
                isExisting = true;
                break;
            }
        }
        
        if(!isExisting)
        {
            
            OrderLine line = new OrderLine();
            line.setOrder(order);
            line.setProduct(product);
            line.setQuantity(quantity);
            line.setPrice(product.getPrice());
            line.setSizeChoose(sizeChoose);
            order.getLines().add(line);
        }
        session.setAttribute("cart", order);
        session.setAttribute("categories", cateDAO.getCategory());
//        request.setAttribute("productID", productID);
//        request.setAttribute("cateID", cateID);
        request.setAttribute("customer",(Customer) request.getSession().getAttribute("customer"));
        request.getRequestDispatcher("productView").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
