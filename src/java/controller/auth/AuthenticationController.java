/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.auth;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modal.Account;
import dal.accountDAO;
import dal.customerDAO;
import static java.lang.System.out;

/**
 *
 * @author sonnt
 */
public class AuthenticationController extends HttpServlet {


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getSession().setAttribute("isSuccess", true);
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String remember = request.getParameter("remember");
        customerDAO customerDAO = new customerDAO();
        accountDAO acDAO = new accountDAO();
        Account account;
        boolean isSuccess = false;
        for (Account e : acDAO.getAccount()) {
        if(username.equals(e.getUsername()) && password.equals(e.getPassword()))
        {   
            account = e;
            request.getSession().setAttribute("account", account);
            //request.getSession().setAttribute("customer", customerDAO.getCustomer(e.getId()));
            if(remember!=null)
            {
                Cookie uUser = new Cookie("username", username);
                uUser.setMaxAge(24*3600);
                Cookie uPass = new Cookie("password", password);
                uPass.setMaxAge(24*3600);
                response.addCookie(uUser);
                response.addCookie(uPass);
            }
            isSuccess=true;
           }
        }
        
         request.getSession().setAttribute("isSuccess", isSuccess);
        if(isSuccess==true) response.sendRedirect("home");
        else response.sendRedirect("login");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
