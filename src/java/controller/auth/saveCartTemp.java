/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.auth;

import dal.customerDAO;
import dal.orderDAO;
import dal.productDAO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modal.Customer;
import modal.Order;
import modal.OrderLine;
import modal.Product;

/**
 *
 * @author anhnb
 */
public class saveCartTemp extends HttpServlet {

   

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        productDAO proDAO = new productDAO();
     orderDAO ordDAO = new orderDAO();
        Product ex;
        customerDAO custDAO = new customerDAO();
        Customer customer = (Customer) request.getSession().getAttribute("customer");
        Order order = (Order) request.getSession().getAttribute("cart");
        order.setID(ordDAO.getOrderCount()+1);
        int custcount = custDAO.getCustomerCount()+1;
        
        if(customer.getId()== custcount){
            order.setCusID(custDAO.getCustomerCount()+1);
            String customername=request.getParameter("customername");
            String address = request.getParameter("address");
            String numberphone=request.getParameter("numberphone");
            customer.setName(customername);
            customer.setAddress(address);
            customer.setNumberphone(numberphone);
            custDAO.setCustomer(customer.getId(), customer.getName(), customer.getAddress(), customer.getNumberphone());
        }
        else{
            order.setCusID(customer.getId());
        }
        
        for(int i=0;i<order.getLines().size();i++){
            int productid = order.getLines().get(i).getProduct().getId();
            int quan =Integer.parseInt(request.getParameter(productid+"")) ;
            order.getLines().get(i).setQuantity(quan);
        }
        ordDAO.setNewOrder(order.getID(), order.getCusID());
       
        ordDAO.setNewOrder(order.getID(), order.getCusID());
        for (OrderLine orLine : order.getLines()) {
            ordDAO.setOrderLine(orLine.getOrder().getID(), orLine.getProduct().getId(), orLine.getQuantity(), orLine.getPrice());
        }
        int quantity =0;
        for (OrderLine line : order.getLines()) {
            ex = proDAO.getProductbyID(line.getProduct().getId()+"", line.getProduct().getCateId()+"");
             switch(line.getSizeChoose()) {
         case 1 :
            quantity = ex.getM() - line.getQuantity();
            break;
         case 2 :
             quantity = ex.getXL()- line.getQuantity();
            break;
         case 3 :
             quantity = ex.getXXL()- line.getQuantity();
            break;
      }
            proDAO.updateProduct(line.getSizeChoose(),quantity , line.getProduct().getId());
        }
        response.sendRedirect("successOrder.jsp");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
