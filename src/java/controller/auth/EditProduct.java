/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.auth;

import dal.categoryDAO;
import dal.productDAO;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modal.Product;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author anhnb
 */
@WebServlet(name = "EditProduct", urlPatterns = {"/Edit"})
public class EditProduct extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        categoryDAO cateDAO = new categoryDAO();
        productDAO proDAO = new productDAO();
        String productID = request.getParameter("productID");

        String cateID = request.getParameter("cateID");
        request.setAttribute("productID", productID);
        request.setAttribute("cateID", cateID);
        request.setAttribute("category", cateDAO.getCategory().get(Integer.parseInt(cateID) - 1));
        request.setAttribute("product", proDAO.getProductbyID(productID, cateID));
        request.setAttribute("categories", cateDAO.getCategory());
        request.getRequestDispatcher("EditProduct.jsp").forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset = UTF-8");
        // Create a factory for disk-based file items
        DiskFileItemFactory factory = new DiskFileItemFactory();

// Configure a repository (to ensure a secure temp location is used)
        ServletContext servletContext = this.getServletConfig().getServletContext();
        File repository = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
        factory.setRepository(repository);

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);

        // Parse the request
        List<FileItem> items;
        try {
            items = upload.parseRequest(request);

            Map<String, String> map = new HashMap<>();
            Iterator<FileItem> iter = items.iterator();
            while (iter.hasNext()) {
                FileItem item = iter.next();

                if (item.isFormField()) {
                    map.put(item.getFieldName(), item.getString());
                    System.out.println(item.getFieldName()+"\t"+item.getString());
                }
            }
            Product product = new Product();
            product.setId(Integer.parseInt(map.get("id")));
            product.setName(map.get("prname"));
            product.setCateId(Integer.parseInt(map.get("cate")));
            product.setPrice(Float.parseFloat(map.get("price")));
            product.setM(Integer.parseInt(map.get("m")));
            product.setXL(Integer.parseInt(map.get("xl")));
            product.setXXL(Integer.parseInt(map.get("xxl")));
            product.setDescription(map.get("description"));
            product.setActive(Integer.parseInt(map.get("active")));
            // System.out.println(product.toString());
            productDAO prDAO = new productDAO();
            prDAO.editProduct(product);
            response.sendRedirect("/aProjectWebb/home");
        } catch (Exception ex) {
            Logger.getLogger(EditProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
