/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import modal.Category;
import modal.Product;

/**
 *
 * @author anhnb
 */
public class productDAO extends DBContext {

    public Product getProductbyID(String id, String cateID) {
        ArrayList<Product> proList = new ArrayList<Product>();
        Product product = null;
        try {
            String sql = "SELECT TOP 1000 [ID]\n"
                    + "      ,[name]\n"
                    + "      ,[cateID]\n"
                    + "      ,[M]\n"
                    + "      ,[XL]\n"
                    + "      ,[XXL]\n"
                    + "      ,[price]\n"
                    + "      ,[description]\n"
                    + " ,[imgPath]"
                    + " ,[active]"
                    + "  FROM [dbo].[Product]"
                    + "  where ID = '" + id + "' and cateID ='" + cateID + "' ";

            PreparedStatement statement = connection.prepareStatement(sql);

            ResultSet rs = statement.executeQuery();
            product = new Product();
            while (rs.next()) {
                product.setId(rs.getInt("ID"));
                product.setName(rs.getString("name"));
                product.setCateId(rs.getInt("cateID"));
                product.setM(rs.getInt("M"));
                product.setXL(rs.getInt("XL"));
                product.setXXL(rs.getInt("XXL"));
                product.setPrice(rs.getFloat("price"));
                product.setDescription(rs.getNString("description"));
                product.setImgPath(rs.getString("imgPath"));
                product.setActive(rs.getInt("active"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(productDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return product;
    }

    public ArrayList<Product> getProductSearch(String search) {
        ArrayList<Product> proList = new ArrayList<Product>();
        Product product;
        String a = search.split(" ")[0];
        String b = search.substring(a.length()).trim();
        try {
            String sql = "  SELECT Product.ID, "
                    + "Product.name, "
                    + "Product.cateID,"
                    + " M, "
                    + "XL, "
                    + "XXL, "
                    + "price, "
                    + "description, "
                    + " imgPath,"
                    + " active,"
                    + "Categories.name AS cateName\n"
                    + "FROM     Categories INNER JOIN\n"
                    + "                  Product ON Categories.ID = Product.cateID\n"
                    + "where Categories.name like '%" + search + "%' or Product.name like '%" + search + "%' \n"
                    + "or( Categories.name like '%" + a + "%' and Product.name like '%" + b + "%')";

            PreparedStatement statement = connection.prepareStatement(sql);

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                product = new Product();
                product.setId(rs.getInt("ID"));
                product.setName(rs.getString("name"));
                product.setCateId(rs.getInt("cateID"));
                product.setM(rs.getInt("M"));
                product.setXL(rs.getInt("XXL"));
                product.setXXL(rs.getInt("XXL"));
                product.setPrice(rs.getFloat("price"));
                product.setDescription("description");
                product.setImgPath(rs.getString("imgPath"));
                product.setActive(rs.getInt("active"));
                if (product.getActive() == 0) {
                    continue;
                }
                proList.add(product);

            }

        } catch (SQLException ex) {
            Logger.getLogger(productDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return proList;
    }

    public void updateProduct(int sizeChoose, int quantity, int id) {

        String sql = null;
        String size = "";
        if (sizeChoose == 1) {
            size = "M";
        }
        if (sizeChoose == 2) {
            size = "XL";
        }
        if (sizeChoose == 3) {
            size = "XXL";
        }
        try {
            sql = " UPDATE Product\n"
                    + "SET " + size + "= " + quantity + " \n"
                    + "WHERE ID=" + id + ";";
            PreparedStatement statement;
            statement = connection.prepareStatement(sql);

            int rs = statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(productDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(sql);
    }

    public void addProduct(Product product) {

        String sql = null;
        String size = "";

        try {
            sql = " INSERT INTO Product(name,cateID,M,XL,XXL,price,description,imgPath,active)\n"
                    + "VALUES (?,?,?,?,?,?,?,?,?);";
            PreparedStatement statement;

            statement = connection.prepareStatement(sql);
            statement.setString(1, product.getName());
            statement.setInt(2, product.getCateId());
            statement.setInt(3, product.getM());
            statement.setInt(4, product.getXL());
            statement.setInt(5, product.getXXL());
            statement.setDouble(6, product.getPrice());
            statement.setString(7, product.getDescription());
            statement.setString(8, product.getImgPath());
             statement.setInt(9, 1);
            statement.execute();
        } catch (SQLException ex) {
            Logger.getLogger(productDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(sql);
    }

    public void editProduct(Product product) {

        String sql = null;
        String size = "";

        try {
            sql = "UPDATE Product\n"
                    + "SET name = ?, cateID= ?,M=?,XL=?,XXL=?,price=?,description=?,active = ? WHERE id = ?;";

            PreparedStatement statement;

            statement = connection.prepareStatement(sql);
            statement.setString(1, product.getName());
            statement.setInt(2, product.getCateId());
            statement.setInt(3, product.getM());
            statement.setInt(4, product.getXL());
            statement.setInt(5, product.getXXL());
            statement.setDouble(6, product.getPrice());
            statement.setString(7, product.getDescription());
            statement.setInt(8, product.getActive());
            statement.setInt(9, product.getId());

            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(productDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(sql);
    }

}
