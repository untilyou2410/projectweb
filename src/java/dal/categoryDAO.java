/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import modal.Category;

/**
 *
 * @author anhnb
 */
public class categoryDAO extends DBContext{
     public ArrayList<Category> getCategory(){
        ArrayList<Category> cateList = new ArrayList<Category>();
        Category category;
         try {
             String sql = "SELECT TOP 1000 [ID]\n" +
                            "      ,[name]\n" +
                            "  FROM [dbo].[Categories]";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                category = new Category();
                category.setId(rs.getInt("ID"));
                category.setName(rs.getString("name"));
                cateList.add(category);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(categoryDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        return cateList;
    }
}
