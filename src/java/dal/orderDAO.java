/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import modal.Customer;
import modal.Order;
import modal.OrderLine;

/**
 *
 * @author anhnb
 */
public class orderDAO extends DBContext {

    public int getOrderCount() {
        int count = 0;
        try {
            String sql = "select COUNT(ID) as count from [Order] ";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                count = rs.getInt("count");
            }

        } catch (SQLException ex) {
            Logger.getLogger(customerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return count;
    }

    public void setNewOrder(int id, int custid) {
        try {
            String sql = "INSERT INTO [Order](ID,CusID)"
                    + "  values(?,?);";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            statement.setInt(2, custid);
            int rs = statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(orderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setOrderLine(int orID, int prID, int quantity, float price) {
        try {
            String sql = " INSERT INTO [OrderLine](orID,prID,quantity,price)\n"
                    + "  VALUES(?,?,?,?);";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, orID);
            statement.setInt(2, prID);
            statement.setInt(3, quantity);
            statement.setFloat(4, price);
            boolean rs = statement.execute();
        } catch (SQLException ex) {
            Logger.getLogger(orderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void getOrder() {
        ArrayList<Order> ordList = new ArrayList<Order>();
        OrderLine line;
        String sql;
        try {
            sql = "SELECT TOP 1000 [orID]\n"
                    + "      ,[prID]\n"
                    + "      ,[quantity]\n"
                    + "      ,[price]\n"
                    + "  FROM [ProjectWeb1].[dbo].[OrderLine]\n"
                    + "  where orID=  ? ";

            PreparedStatement statement = connection.prepareStatement(sql);
            boolean rs = statement.execute();

        } catch (SQLException ex) {
            Logger.getLogger(orderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Map getOrder1() {
        Map<Integer, String> map = new HashMap<>();
        String sql = "";
        try {
            sql = "select ord.ID,c.name from [dbo].[Order] as ord inner join \n"
                    + " Customer as c on ord.CusID = c.ID ";

            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                map.put(rs.getInt("ID"), rs.getString("name"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(orderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return map;
    }

    public  ArrayList<OrderLine> viewOrderDetail(int orID ) {
        ArrayList<OrderLine> ordList = new ArrayList<OrderLine>();
        OrderLine line = null;
        String sql;
        try {
            sql = "select OrderLine.orID, Product.name as prName, Categories.name,OrderLine.quantity,OrderLine.price\n"
                    + "from OrderLine, Categories, [dbo].[Order], Product\n"
                    + "where OrderLine.orID = [dbo].[Order].ID and OrderLine.prID = Product.ID and Categories.ID = Product.cateID and OrderLine.orID = ?";

            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, orID);
             ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                line = new OrderLine();
                line.setPrName(rs.getString("prName")+" "+rs.getString("name"));
                line.setQuantity(Integer.parseInt(rs.getString("quantity")));
                line.setPrice(Float.parseFloat(rs.getString("price")));
                ordList.add(line);
            }

        } catch (SQLException ex) {
            Logger.getLogger(orderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ordList;
    }

}
