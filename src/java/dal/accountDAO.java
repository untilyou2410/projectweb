/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import modal.Account;
import modal.Category;
import modal.Product;

/**
 *
 * @author anhnb
 */
public class accountDAO extends DBContext {

    public ArrayList<Account> getAccount() {
        Account acc;
        ArrayList<Account> accList = new ArrayList<Account>();

        try {
            String sql = "SELECT TOP 1000 [ID]\n"
                    + "      ,[username]\n"
                    + "      ,[password]\n"
                    + "      ,[isAdmin]\n"
                    + "  FROM [dbo].[account]";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                acc = new Account();
                acc.setId(rs.getInt("ID"));
                acc.setUsername(rs.getString("username"));
                acc.setPassword(rs.getString("password"));
                acc.setIsAdmin(rs.getBoolean("isAdmin"));
                accList.add(acc);
            }

        } catch (SQLException ex) {
            Logger.getLogger(accountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return accList;
    }

    public void addAccount(Account account) {

        String sql = null;
        String size = "";

        try {
            sql = "  insert into account(username,password,isAdmin)\n"
                    + "  values(?,?,0);";
            PreparedStatement statement;

            statement = connection.prepareStatement(sql);
            statement.setString(1,account.getUsername());
            statement.setString(2,account.getPassword());

            statement.execute();
        } catch (SQLException ex) {
            Logger.getLogger(productDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(sql);
    }

}
