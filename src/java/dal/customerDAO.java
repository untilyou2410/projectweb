/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import modal.Customer;

/**
 *
 * @author anhnb
 */
public class customerDAO extends DBContext{
      public Customer getCustomer(int accID){
        ArrayList<Customer> custList = new ArrayList<Customer>();
        Customer customer = null;
         try {
             String sql = "SELECT TOP 1000 [ID]\n" +
                            "      ,[name]\n" +
                            "      ,[address]\n" +
                            "      ,[numberphone]\n" +
                            "      ,[accID]\n" +
                            "  FROM [dbo].[Customer]"+
                            "  where accID= "+accID+" ";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                customer = new Customer();
                customer.setId(rs.getInt("ID"));
                customer.setName(rs.getString("name"));
                customer.setAddress(rs.getNString("address"));
                customer.setNumberphone(rs.getString("numberphone"));
                customer.setAccId(rs.getInt("accID"));
                custList.add(customer);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(customerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        return customer;
    }
    public int getCustomerCount(){
        int count=0;
         try {
             String sql = "select COUNT(ID) as count from [Customer]";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
              count = rs.getInt("count");
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(customerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        return count;
    }
    public void setCustomer(int id,String name,String address,String numberphone){
  
          try {
               String sql ="INSERT INTO [Customer](ID,name,address,numberphone)" +
                    "VALUES (?, ?, ?, ?);";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            statement.setString(2, name);
            statement.setString(3, address);
            statement.setString(4, numberphone);
            boolean rs = statement.execute();
        } catch (SQLException ex) {
            Logger.getLogger(customerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
