USE [ProjectWeb1]
GO
/****** Object:  Table [dbo].[account]    Script Date: 11/15/2020 9:36:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[account](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](50) NOT NULL,
	[password] [varchar](50) NOT NULL,
	[isAdmin] [bit] NOT NULL,
 CONSTRAINT [PK_account] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 11/15/2020 9:36:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[ID] [int] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customer]    Script Date: 11/15/2020 9:36:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[ID] [int] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[address] [nvarchar](50) NOT NULL,
	[numberphone] [int] NOT NULL,
	[accID] [int] NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Order]    Script Date: 11/15/2020 9:36:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[ID] [int] NOT NULL,
	[CusID] [int] NOT NULL,
	[StaffID] [int] NULL,
	[shipID] [int] NULL,
	[date] [date] NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderLine]    Script Date: 11/15/2020 9:36:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderLine](
	[orID] [int] NOT NULL,
	[prID] [int] NOT NULL,
	[quantity] [int] NULL,
	[price] [float] NULL,
 CONSTRAINT [PK_OrderLine] PRIMARY KEY CLUSTERED 
(
	[orID] ASC,
	[prID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Product]    Script Date: 11/15/2020 9:36:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[cateID] [int] NOT NULL,
	[M] [int] NULL,
	[XL] [int] NULL,
	[XXL] [int] NULL,
	[price] [float] NULL,
	[description] [nvarchar](300) NULL,
	[imgPath] [varchar](255) NULL,
	[active] [int] NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Shipper]    Script Date: 11/15/2020 9:36:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shipper](
	[ID] [int] NOT NULL,
	[name] [nvarchar](50) NULL,
	[numberphone] [int] NULL,
 CONSTRAINT [PK_Shipper] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Staff]    Script Date: 11/15/2020 9:36:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Staff](
	[ID] [int] NOT NULL,
	[name] [nvarchar](50) NULL,
	[address] [nvarchar](50) NULL,
	[numberphone] [int] NULL,
 CONSTRAINT [PK_Staff] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[account] ON 

INSERT [dbo].[account] ([ID], [username], [password], [isAdmin]) VALUES (1, N'tienanh', N'123', 0)
INSERT [dbo].[account] ([ID], [username], [password], [isAdmin]) VALUES (2, N'van', N'321', 1)
INSERT [dbo].[account] ([ID], [username], [password], [isAdmin]) VALUES (4, N'avcc', N'axxa', 0)
INSERT [dbo].[account] ([ID], [username], [password], [isAdmin]) VALUES (5, N'tienanh1', N'123', 0)
SET IDENTITY_INSERT [dbo].[account] OFF
INSERT [dbo].[Categories] ([ID], [name]) VALUES (1, N'Sweatshirts')
INSERT [dbo].[Categories] ([ID], [name]) VALUES (2, N'T-shirt')
INSERT [dbo].[Categories] ([ID], [name]) VALUES (3, N'jeans')
INSERT [dbo].[Categories] ([ID], [name]) VALUES (4, N'dress')
INSERT [dbo].[Categories] ([ID], [name]) VALUES (5, N'shirt')
INSERT [dbo].[Customer] ([ID], [name], [address], [numberphone], [accID]) VALUES (1, N'C1', N'mộ đạo', 1312, 1)
INSERT [dbo].[Customer] ([ID], [name], [address], [numberphone], [accID]) VALUES (2, N'C2', N'quế võ', 4332, NULL)
INSERT [dbo].[Customer] ([ID], [name], [address], [numberphone], [accID]) VALUES (3, N'C3', N'bắc ninh', 4323, 2)
INSERT [dbo].[Customer] ([ID], [name], [address], [numberphone], [accID]) VALUES (4, N'C4', N'haha', 2222, NULL)
INSERT [dbo].[Customer] ([ID], [name], [address], [numberphone], [accID]) VALUES (5, N'batienanh', N'quees vox', 554477, NULL)
INSERT [dbo].[Customer] ([ID], [name], [address], [numberphone], [accID]) VALUES (6, N'tieeeee', N'anhhh', 555555, NULL)
INSERT [dbo].[Customer] ([ID], [name], [address], [numberphone], [accID]) VALUES (7, N'C1', N'má» Äáº¡o', 1312, NULL)
INSERT [dbo].[Customer] ([ID], [name], [address], [numberphone], [accID]) VALUES (8, N'sdsad', N'dasdas', 32111, NULL)
INSERT [dbo].[Customer] ([ID], [name], [address], [numberphone], [accID]) VALUES (9, N'nulldd', N'nulldsadsa', 32112, NULL)
INSERT [dbo].[Customer] ([ID], [name], [address], [numberphone], [accID]) VALUES (10, N'323', N'sadas', 32131, NULL)
INSERT [dbo].[Customer] ([ID], [name], [address], [numberphone], [accID]) VALUES (11, N'nullddddd', N'nulldaaaa', 32111, NULL)
INSERT [dbo].[Customer] ([ID], [name], [address], [numberphone], [accID]) VALUES (12, N'sadsadas', N'dasdasdas', 312111, NULL)
INSERT [dbo].[Customer] ([ID], [name], [address], [numberphone], [accID]) VALUES (13, N'công phong', N'dsasda', 31111, NULL)
INSERT [dbo].[Customer] ([ID], [name], [address], [numberphone], [accID]) VALUES (14, N'sonnt', N'hà nội', 32222, NULL)
INSERT [dbo].[Customer] ([ID], [name], [address], [numberphone], [accID]) VALUES (15, N'â', N'd', 321321, NULL)
INSERT [dbo].[Customer] ([ID], [name], [address], [numberphone], [accID]) VALUES (16, N'tes1', N'111', 231232321, NULL)
INSERT [dbo].[Customer] ([ID], [name], [address], [numberphone], [accID]) VALUES (17, N'admin', N'admin', 2321, NULL)
INSERT [dbo].[Customer] ([ID], [name], [address], [numberphone], [accID]) VALUES (18, N'aaa', N'abc', 2133333, NULL)
INSERT [dbo].[Customer] ([ID], [name], [address], [numberphone], [accID]) VALUES (19, N'tienanhtest', N'tienanhtest', 12321, NULL)
INSERT [dbo].[Order] ([ID], [CusID], [StaffID], [shipID], [date]) VALUES (1, 9, NULL, NULL, NULL)
INSERT [dbo].[Order] ([ID], [CusID], [StaffID], [shipID], [date]) VALUES (2, 1, NULL, NULL, NULL)
INSERT [dbo].[Order] ([ID], [CusID], [StaffID], [shipID], [date]) VALUES (3, 10, NULL, NULL, NULL)
INSERT [dbo].[Order] ([ID], [CusID], [StaffID], [shipID], [date]) VALUES (4, 11, NULL, NULL, NULL)
INSERT [dbo].[Order] ([ID], [CusID], [StaffID], [shipID], [date]) VALUES (5, 12, NULL, NULL, NULL)
INSERT [dbo].[Order] ([ID], [CusID], [StaffID], [shipID], [date]) VALUES (6, 13, NULL, NULL, NULL)
INSERT [dbo].[Order] ([ID], [CusID], [StaffID], [shipID], [date]) VALUES (7, 1, NULL, NULL, NULL)
INSERT [dbo].[Order] ([ID], [CusID], [StaffID], [shipID], [date]) VALUES (8, 14, NULL, NULL, NULL)
INSERT [dbo].[Order] ([ID], [CusID], [StaffID], [shipID], [date]) VALUES (9, 15, NULL, NULL, NULL)
INSERT [dbo].[Order] ([ID], [CusID], [StaffID], [shipID], [date]) VALUES (10, 1, NULL, NULL, NULL)
INSERT [dbo].[Order] ([ID], [CusID], [StaffID], [shipID], [date]) VALUES (11, 16, NULL, NULL, NULL)
INSERT [dbo].[Order] ([ID], [CusID], [StaffID], [shipID], [date]) VALUES (12, 17, NULL, NULL, NULL)
INSERT [dbo].[Order] ([ID], [CusID], [StaffID], [shipID], [date]) VALUES (13, 18, NULL, NULL, NULL)
INSERT [dbo].[Order] ([ID], [CusID], [StaffID], [shipID], [date]) VALUES (14, 19, NULL, NULL, NULL)
INSERT [dbo].[OrderLine] ([orID], [prID], [quantity], [price]) VALUES (1, 4, 2, 550000)
INSERT [dbo].[OrderLine] ([orID], [prID], [quantity], [price]) VALUES (2, 7, 3, 600000)
INSERT [dbo].[OrderLine] ([orID], [prID], [quantity], [price]) VALUES (3, 1, 2, 300000)
INSERT [dbo].[OrderLine] ([orID], [prID], [quantity], [price]) VALUES (4, 2, 2, 400000)
INSERT [dbo].[OrderLine] ([orID], [prID], [quantity], [price]) VALUES (5, 1, 2, 300000)
INSERT [dbo].[OrderLine] ([orID], [prID], [quantity], [price]) VALUES (5, 2, 2, 400000)
INSERT [dbo].[OrderLine] ([orID], [prID], [quantity], [price]) VALUES (6, 7, 1, 600000)
INSERT [dbo].[OrderLine] ([orID], [prID], [quantity], [price]) VALUES (7, 7, 3, 600000)
INSERT [dbo].[OrderLine] ([orID], [prID], [quantity], [price]) VALUES (7, 9, 3, 450000)
INSERT [dbo].[OrderLine] ([orID], [prID], [quantity], [price]) VALUES (8, 4, 1, 550000)
INSERT [dbo].[OrderLine] ([orID], [prID], [quantity], [price]) VALUES (9, 11, 3, 500000)
INSERT [dbo].[OrderLine] ([orID], [prID], [quantity], [price]) VALUES (10, 11, 6, 500000)
INSERT [dbo].[OrderLine] ([orID], [prID], [quantity], [price]) VALUES (11, 12, 1, 350000)
INSERT [dbo].[OrderLine] ([orID], [prID], [quantity], [price]) VALUES (12, 3, 3, 500000)
INSERT [dbo].[OrderLine] ([orID], [prID], [quantity], [price]) VALUES (12, 8, 1, 650000)
INSERT [dbo].[OrderLine] ([orID], [prID], [quantity], [price]) VALUES (13, 11, 1, 500000)
INSERT [dbo].[OrderLine] ([orID], [prID], [quantity], [price]) VALUES (14, 12, 13, 350000)
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([ID], [name], [cateID], [M], [XL], [XXL], [price], [description], [imgPath], [active]) VALUES (1, N'test23333', 1, 1, 1, 1, 1, N'asds', N'1_3.jpg', 23)
INSERT [dbo].[Product] ([ID], [name], [cateID], [M], [XL], [XXL], [price], [description], [imgPath], [active]) VALUES (2, N'LOOSE', 4, 1, 2, 1, 400000, N'Áo nỉ dài tay nữ dáng loose in graphic Busicker Cuteness Caution.
- Chất liệu: nỉ
- Size mẫu mặc: M
- Số đo mẫu: 50kg, 80 - 60 - 90, 167 cm', N'2_4.jpg', 1)
INSERT [dbo].[Product] ([ID], [name], [cateID], [M], [XL], [XXL], [price], [description], [imgPath], [active]) VALUES (3, N'MICKEY HUG ME', 4, 0, -1, 1, 500000, NULL, N'3_4.jpg', 1)
INSERT [dbo].[Product] ([ID], [name], [cateID], [M], [XL], [XXL], [price], [description], [imgPath], [active]) VALUES (4, N'dài', 3, 2, 3, 5, 550000, N'Áo nỉ dài tay nữ dáng loose in graphic Busicker Cuteness Caution.
- Chất liệu: nỉ
- Size mẫu mặc: M
- Số đo mẫu: 50kg, 80 - 60 - 90, 167 cm', N'4_3.jpg', 1)
INSERT [dbo].[Product] ([ID], [name], [cateID], [M], [XL], [XXL], [price], [description], [imgPath], [active]) VALUES (5, N'nam nu', 5, 0, 0, -3, 300000, NULL, N'5_5.jpg', 1)
INSERT [dbo].[Product] ([ID], [name], [cateID], [M], [XL], [XXL], [price], [description], [imgPath], [active]) VALUES (6, N'without hoods male ', 1, 2, 3, 4, 700000, N'Áo nỉ dài tay nữ dáng loose in graphic Busicker Cuteness Caution.
- Chất liệu: nỉ
- Size mẫu mặc: M
- Số đo mẫu: 50kg, 80 - 60 - 90, 167 cm', N'6_1.jpg', 1)
INSERT [dbo].[Product] ([ID], [name], [cateID], [M], [XL], [XXL], [price], [description], [imgPath], [active]) VALUES (7, N'without hoods male MARVEL COMICSGROUP', 1, 0, 1, 6, 600000, NULL, N'7_1.jpg', 1)
INSERT [dbo].[Product] ([ID], [name], [cateID], [M], [XL], [XXL], [price], [description], [imgPath], [active]) VALUES (8, N'without hoods female CAPTION AMERICA', 1, 2, 2, 1, 650000, NULL, N'8_1.jpg', 1)
INSERT [dbo].[Product] ([ID], [name], [cateID], [M], [XL], [XXL], [price], [description], [imgPath], [active]) VALUES (9, N'without hoods male LOGO L BACK TO THE ROOT', 1, 2, 7, 1, 450000, N'Áo nỉ dài tay nữ dáng loose in graphic Busicker Cuteness Caution.
- Chất liệu: nỉ
- Size mẫu mặc: M
- Số đo mẫu: 50kg, 80 - 60 - 90, 167 cm', N'9_1.jpg', 1)
INSERT [dbo].[Product] ([ID], [name], [cateID], [M], [XL], [XXL], [price], [description], [imgPath], [active]) VALUES (10, N'long sleeve female BUSTICKERS DRAMA QUEEN', 2, 2, 4, 1, 550000, NULL, N'10_2.jpg', 1)
INSERT [dbo].[Product] ([ID], [name], [cateID], [M], [XL], [XXL], [price], [description], [imgPath], [active]) VALUES (11, N'long sleeve female IRON MAN', 2, -4, 5, 2, 500000, NULL, N'11_2.jpg', 1)
INSERT [dbo].[Product] ([ID], [name], [cateID], [M], [XL], [XXL], [price], [description], [imgPath], [active]) VALUES (12, N'long sleeve female BOO LOGO', 2, -12, 6, 1, 350000, NULL, N'12_2.jpg', 1)
INSERT [dbo].[Product] ([ID], [name], [cateID], [M], [XL], [XXL], [price], [description], [imgPath], [active]) VALUES (13, N'abc', 1, 2, 3, 4, 5000, N'aaaaa', N'path', 0)
INSERT [dbo].[Product] ([ID], [name], [cateID], [M], [XL], [XXL], [price], [description], [imgPath], [active]) VALUES (16, N'test2222', 1, 1, 1, 1, 1, N'', N'av.jpg', 0)
INSERT [dbo].[Product] ([ID], [name], [cateID], [M], [XL], [XXL], [price], [description], [imgPath], [active]) VALUES (20, N'', 1, 1, 0, 2, 0, N'abcd', N'abc.png', 0)
INSERT [dbo].[Product] ([ID], [name], [cateID], [M], [XL], [XXL], [price], [description], [imgPath], [active]) VALUES (21, N'Trang', 1, 1, 5, 2, 300000, N'hihi do ngok', N'1.webp', 1)
INSERT [dbo].[Product] ([ID], [name], [cateID], [M], [XL], [XXL], [price], [description], [imgPath], [active]) VALUES (22, N'test edit', 4, 4, 10, 2, 6, N'desceq dddd', N'6.webp', 0)
INSERT [dbo].[Product] ([ID], [name], [cateID], [M], [XL], [XXL], [price], [description], [imgPath], [active]) VALUES (23, N'test23333', 1, 1, 1, 1, 1, N'xxxxxxx', N'15.webp', 0)
INSERT [dbo].[Product] ([ID], [name], [cateID], [M], [XL], [XXL], [price], [description], [imgPath], [active]) VALUES (24, NULL, 1, 3, 0, 2, 0, N'', N'3.webp', 1)
INSERT [dbo].[Product] ([ID], [name], [cateID], [M], [XL], [XXL], [price], [description], [imgPath], [active]) VALUES (25, N'test1', 1, 3, 2, 2, 333, N'', N'av.jpg', 0)
INSERT [dbo].[Product] ([ID], [name], [cateID], [M], [XL], [XXL], [price], [description], [imgPath], [active]) VALUES (26, NULL, 3, 3, 0, 3, 0, N'', N'4.webp', NULL)
INSERT [dbo].[Product] ([ID], [name], [cateID], [M], [XL], [XXL], [price], [description], [imgPath], [active]) VALUES (27, N'dddd', 3, -3, 0, -4, 0, N'ddddd', N'3.webp', 1)
INSERT [dbo].[Product] ([ID], [name], [cateID], [M], [XL], [XXL], [price], [description], [imgPath], [active]) VALUES (28, N'test1', 2, 5, 0, 4, 0, N'testdesc', N'6.webp', 1)
SET IDENTITY_INSERT [dbo].[Product] OFF
INSERT [dbo].[Shipper] ([ID], [name], [numberphone]) VALUES (1, N'ShipA', 231)
INSERT [dbo].[Shipper] ([ID], [name], [numberphone]) VALUES (2, N'ShipB', 2132)
INSERT [dbo].[Shipper] ([ID], [name], [numberphone]) VALUES (3, N'ShipC', 321)
INSERT [dbo].[Staff] ([ID], [name], [address], [numberphone]) VALUES (1, N'Staff1', N'hihi', 32132)
INSERT [dbo].[Staff] ([ID], [name], [address], [numberphone]) VALUES (2, N'Staff2', N'sada', 423)
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_account] FOREIGN KEY([accID])
REFERENCES [dbo].[account] ([ID])
GO
ALTER TABLE [dbo].[Customer] CHECK CONSTRAINT [FK_Customer_account]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Customer] FOREIGN KEY([CusID])
REFERENCES [dbo].[Customer] ([ID])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Customer]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Shipper] FOREIGN KEY([shipID])
REFERENCES [dbo].[Shipper] ([ID])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Shipper]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Staff] FOREIGN KEY([StaffID])
REFERENCES [dbo].[Staff] ([ID])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Staff]
GO
ALTER TABLE [dbo].[OrderLine]  WITH CHECK ADD  CONSTRAINT [FK_OrderLine_Order] FOREIGN KEY([orID])
REFERENCES [dbo].[Order] ([ID])
GO
ALTER TABLE [dbo].[OrderLine] CHECK CONSTRAINT [FK_OrderLine_Order]
GO
ALTER TABLE [dbo].[OrderLine]  WITH CHECK ADD  CONSTRAINT [FK_OrderLine_Product] FOREIGN KEY([prID])
REFERENCES [dbo].[Product] ([ID])
GO
ALTER TABLE [dbo].[OrderLine] CHECK CONSTRAINT [FK_OrderLine_Product]
GO
