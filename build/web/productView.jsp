<%-- 
    Document   : productView
    Created on : Oct 31, 2019, 9:17:40 PM
    Author     : anhnb
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="modal.Account"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <title>Shop Homepage - Start Bootstrap Template</title>

        <!-- Bootstrap core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="css/shop-homepage.css" rel="stylesheet">
    </head>
    <body>
        
               <!-- Navigation -->
  <%@include file="header.jsp" %>
           <br></br>
        

        <div class="container">

            <hr>
            <div class="card">
                <div class="row">
                    <aside class="col-sm-5 border-right">
                        <article class="gallery-wrap"> 
                            <div class="img-big-wrap">
                                <div > <a href="#"><img style="max-width: -webkit-fill-available;" src="image/${requestScope.product.getImgPath()}"></a></div>
                            <!-- slider-product.// -->
                            <!-- slider-nav.// -->
                        </article> <!-- gallery-wrap .end// -->
                    </aside>
                            
                   <!-- sss-->        
                    <aside class="col-sm-7">
                        <article class="card-body p-5">
                            <h3 class="title mb-3">${requestScope.category.getName()} ${requestScope.product.getName()} </h3>

                            <p class="price-detail-wrap"> 
                                <span class="price h3 text-warning"> 
                                    <span class="num">${requestScope.product.getPrice()}</span><span class="currency"> VNĐ</span>
                                </span>
                            </p> <!-- price-detail-wrap .// -->
                            <dl class="item-property">
                                <dt>Description</dt>
                                <dd><p>   ${requestScope.product.getDescription()}          </p></dd>
                            </dl>

                            <dl class="param param-feature">
                                <dt>Model#</dt>
                                <img class="card-img-top" src="image/sizema_1.png" alt="">
                                <dd></dd>
                            </dl>  <!-- item-property-hor .// -->


                            <hr>    <form id="frm" action="cart" method="post">
                                <input type="hidden" value="${requestScope.product.getId()}" name="productID">
                                <input type="hidden" value="${requestScope.product.getCateId()}" name="cateID">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <dl class="param param-inline">
                                            <dt>Quantity: </dt>
                                            <dd>
                                                <input type="number" name="quantity" min="1" max="5" step="1" value="1">

                                            </dd>
                                        </dl>  <!-- item-property .// -->
                                    </div> <!-- col.// -->
                         

                            <div class="col-sm-7">
                                <dl class="param param-inline">
                                    
                                    <dt>Size: </dt>
                                    <dd>
                                    <c:if test = "${requestScope.product.getM() > 0}">
                                        <p> ${requestScope.prodcut.getM()} </p>
                                        <label class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" checked=""  name="inlineRadioOptions" id="inlineRadio2" value="1">
                                            <span class="form-check-label">M</span>
                                        </label>
                                          </c:if>
                                        <c:if test = "${requestScope.product.getXL() > 0}">
                                        <label class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" <c:if test="${ requestScope.product.getM()<=0}"> checked="" </c:if> name="inlineRadioOptions" id="inlineRadio2" value="2">
                                            <span class="form-check-label">XL</span>
                                        </label>
                                         </c:if>
                                         <c:if test = "${requestScope.product.getXXL() > 0}">
                                        <label class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" <c:if test="${(requestScope.product.getM() <=0) && (requestScope.product.getXL()<=0)}"> checked=""  </c:if> name="inlineRadioOptions" id="inlineRadio2" value="3">
                                            <span class="form-check-label">XXL</span>
                                        </label>
                                         </c:if>
                                    </dd>
                                </dl>  <!-- item-property .// -->
                            </div> <!-- col.// -->
                            </div> <!-- row.// -->
                               </form>    
                            <hr>

                            <a href="cart?method=doGet" class="btn btn-lg btn-primary text-uppercase"> Buy now </a>
                            <input type="button" onclick="document.getElementById('frm').submit();" class="btn btn-lg btn-outline-primary text-uppercase" value="add to cart"> <i class="fas fa-shopping-cart" value="Add to cart"></i>  </input>
                        </article> <!-- card-body.// -->
                    </aside> <!-- col.// -->
                </div> <!-- row.// -->
            </div> <!-- card.// -->


        </div>
        <!--container.//-->


        <br><br><br>
        <article class="bg-secondary mb-3">  
            <div class="card-body text-center">
                <h4 class="text-white">HTML UI KIT <br> Ready to use Bootstrap 4 components and templates </h4>
            </div>
            <br><br><br>
        </article>
    </body>
</html>
