<%-- 
    Document   : list
    Created on : Oct 2, 2019, 1:39:22 PM
    Author     : sonnt
--%>

<%@page import="modal.Account"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <% 
            Account account = (Account)session.getAttribute("account");
        %>
    </head>
    <body>
        <table border="1px">
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Gender</th>
                <th>Dob</th>
                <th>Department</th>
            </tr>
            <c:forEach items="${requestScope.emps}" var="e" varStatus="loop" >
                <tr ${loop.index mod 2 == 0?"style=\"background-color: gray;\"":""} >
                    <td>${e.id}</td>
                    <td>${e.name}</td>
                    <td>${e.gender}</td>
                    <td><fmt:formatDate type = "date" 
                                    value = "${e.dob}" /></td>
                    <td>${e.dept.name}</td>
                </tr>
            </c:forEach>
        </table>    
        
        
        Hello <%=account.getUsername()%> <br/>
        Click <a href="logout">Here</a> to logout
    </body>
</html>
