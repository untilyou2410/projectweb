<%-- 
    Document   : viewOrder
    Created on : Sep 19, 2020, 1:28:50 PM
    Author     : anhnb
--%>

<%@page import="java.util.Map"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <title>Shop Homepage - Start Bootstrap Template</title>

        <!-- Bootstrap core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="css/shop-homepage.css" rel="stylesheet">
        <%
            Map<Integer, String> map = (Map<Integer, String>) request.getAttribute("infor");
        %>
        <style>
            table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
            }

            tr:nth-child(even) {
                background-color: #dddddd;
            }
        </style>
    </head>
    <body>
        <!-- Navigation -->
        <%@include file="header.jsp" %>
        <br></br>
        <table class="">
            <tr>
                <th>Order ID</th>
                <th>Customer Name</th>
                <th></th>
            </tr>
            <%    for (Map.Entry m : map.entrySet()) {
            %>
            <tr>
                <th><%=m.getKey()%></th>
                <th><%=m.getValue()%></th>
                <th> <a href="ViewOrderDetail?orID=<%=m.getKey()%>">View Detail</a> </th>
            </tr>
            <%}%>

        </table>


    </body>
</html>
