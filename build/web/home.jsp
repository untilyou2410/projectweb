<%-- 
    Document   : home
    Created on : Oct 29, 2019, 8:27:54 PM
    Author     : anhnb
--%>

<%@page import="modal.Product"%>
<%@page import="modal.Category"%>
<%@page import="java.util.ArrayList"%>
<%@page import="modal.Account"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>



<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <title>Shop Homepage - Start Bootstrap Template</title>

        <!-- Bootstrap core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="css/shop-homepage.css" rel="stylesheet">
        <%
            ArrayList<Category> categories = (ArrayList<Category>) request.getAttribute("categories");
            ArrayList<Product> products = (ArrayList<Product>) request.getAttribute("products");
            Account acc = (Account) request.getSession().getAttribute("account");
             String pe = "productView";
            if (acc != null) {
               
                if (acc.isIsAdmin()) {
                    pe = "Edit";
                }
            }
        %>
    </head>

    <body>
        <%@include file="header.jsp" %>
        <br></br>
        <!-- Page Content -->
        <div class="container">

            <div class="row">

                <div class="col-lg-3">

                    <h1 class="my-4">   </h1>
                    </br>
                    <div class="list-group">
                        <%for (Category cat : categories) {%>
                        <a href="home?search=<%=cat.getName()%>" class="list-group-item"><%=cat.getName()%></a>

                        <%}%>


                    </div>

                </div>
                <!-- /.col-lg-3 -->

                <div class="col-lg-9">

                    <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">

                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <img class="d-block img-fluid" src="image/home1.jpg" alt="First slide">

                            </div>
                            <div class="carousel-item">
                                <img class="d-block img-fluid" src="image/home2.jpg" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block img-fluid" src="image/home3.jpg" alt="Third slide">
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <%for (Product pro : products) {
                                String path = "image/" + pro.getImgPath();
                                String productname = "";
                                for (Category cate : categories) {
                                    if (cate.getId() == pro.getCateId()) {
                                        productname = cate.getName() + " " + pro.getName();
                                    }
                                }
                        %>
                        <div class="col-lg-4 col-md-6 mb-4">
                            <div class="card h-100">
                                <a href="<%=pe%>?productID=<%=pro.getId()%>&cateID=<%=pro.getCateId()%>">
                                    <img class="card-img-top" src="<%=path%>" alt="">
                                </a>
                                <div class="card-body">
                                    <h4 class="card-title">
                                        <a href="<%=pe%>?productID=<%=pro.getId()%>&cateID=<%=pro.getCateId()%>">
                                            <%=productname%> 
                                        </a>
                                    </h4>
                                    <h5><%=pro.getPrice()%>₫</h5>
                                </div>

                            </div>


                        </div>   

                        <%}%>
                    </div>
                    <!-- /.row -->

                </div>
                <!-- /.col-lg-9 -->

            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->

        <!-- Footer -->
        <footer class="py-5 bg-dark">
            <div class="container">
                <p class="m-0 text-center text-white">Copyright &copy; Your Website 2019</p>
            </div>
            <!-- /.container -->
        </footer>

        <!-- Bootstrap core JavaScript -->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    </body>

</html>
