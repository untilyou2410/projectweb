<%-- 
    Document   : viewOrder
    Created on : Sep 19, 2020, 1:28:50 PM
    Author     : anhnb
--%>

<%@page import="modal.OrderLine"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <title>Shop Homepage - Start Bootstrap Template</title>

        <!-- Bootstrap core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="css/shop-homepage.css" rel="stylesheet">
        <%
            ArrayList<OrderLine> list = (ArrayList<OrderLine>) request.getAttribute("infor");
            int total =0;
        %>
        <style>
            table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
            }

            tr:nth-child(even) {
                background-color: #dddddd;
            }
        </style>
    </head>
    <body>
        <!-- Navigation -->
        <%@include file="header.jsp" %>
        <br></br>
        <br></br>
        <br></br>
        <table class="">
            <tr>
                <th>Product name</th>
                <th>Quantity</th>
                <th>Amount</th>
                <th>Price</th>
          
            </tr>
            <%    for (OrderLine ol : list) {
                total +=  ol.getAmount();
            %>
            <tr>
                <th><%=ol.getPrName()%></th>
                <th><%=ol.getQuantity()%></th>
                <th><%=ol.getAmount()%></th>
                <th><%=ol.getPrice()%></th>
                
            </tr>
            <%}%>
            <tr>
                <th>Total </th>
                <th> <%=total%></th>
            </tr>    
        </table>


    </body>
</html>
