<%-- 
    Document   : header
    Created on : Sep 19, 2020, 1:33:39 PM
    Author     : anhnb
--%>

<%@page import="modal.Account"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <%
      Account ac = (Account) request.getSession().getAttribute("account");
    
  %>
    </head>
    <body>
       
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="home">Cloud Shop</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="home">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <form action="home" method="get">
              <div>
         <input name="search" type="text" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
        <div class="input-group-append">
            <button class="btn btn-primary" type="submit">
         <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
        </form>     
          <%if(ac!=null) {%>
          <li class="nav-item">
            <a class="nav-link" href="logout">Logout</a>
          </li>
          <li class="nav-item">
            <p class="nav-link"> <%=ac.getUsername()%>  </p>
          </li>
          <%if (ac.isIsAdmin()){
          %>
          <li class="nav-item">
             <a class="nav-link" href="viewOrder">View Order</a>
          </li>
             <li class="nav-item">
             <a class="nav-link" href="UploadFil">Add Product</a>
          </li>
              <%}%>
          <%} else{%>
          <li class="nav-item">
            <a class="nav-link" href="http://localhost:8080/aProjectWebb/login">Login</a>
          </li>
          
          <%}%>
           <li class="nav-item">
            <a class="nav-link" href="cart?method=doGet">Cart</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
    </body>
</html>
