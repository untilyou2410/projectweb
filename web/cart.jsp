<%-- 
    Document   : cart
    Created on : Oct 29, 2019, 9:12:25 PM
    Author     : anhnb
--%>

<%@page import="modal.Account"%>
<%@page import="modal.Customer"%>
<%@page import="java.util.ArrayList"%>
<%@page import="modal.Category"%>
<%@page import="modal.OrderLine"%>
<%@page import="modal.Order"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <title>Shop Homepage - Start Bootstrap Template</title>

        <!-- Bootstrap core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->


        <style>
            table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            td, th {
                border: 1px solid #726262;
                text-align: left;
                padding: 8px;
            }

            tr:nth-child(even) {
                background-color: #dddddd;
            }
        </style>

        <%
            Order order = (Order) session.getAttribute("cart");
            int linex = 0;
            if(order!=null){
                linex=order.getLines().size();
            }
            ArrayList<Category> categories = (ArrayList<Category>) session.getAttribute("categories");
            int count = 1;
            Customer customer = (Customer) session.getAttribute("customer");
        %>


    </head>

    <body >

        <!-- Navigation -->
        <%@include file="header.jsp" %>
        <br></br>
        <br></br>
        <br></br>

        <form id="contact" action="saveCartTemp" method="get">
            <table id="table1">
                <tr>
                    <td>  
                        <%if (categories != null) {%>
                        
                        <table>
                            <tr>
                                <th>Product</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Amount</th>
                            </tr>
                            <%for (OrderLine e : order.getLines()) {
                                    String path = "image/" + e.getProduct().getId() + "_" + e.getProduct().getCateId() + ".jpg";
                                    String productname = "";
                                    for (Category cate : categories) {
                                        if (cate.getId() == e.getProduct().getCateId()) {
                                            productname = cate.getName() + " " + e.getProduct().getName();
                                        }
                                    }%>
                            <tr>

                                <td><a href="productView?productID=<%=e.getProduct().getId()%>&cateID=<%=e.getProduct().getCateId()%>">
                                        <%=productname%> 
                                    </a></td>
                                <td>
                            <dd>
                                <input type="text"  onchange=myFunction(<%=e.getProduct().getPrice()%>,document.getElementById("<%=e.getProduct().getId()%>").value,"amount<%=count%>") 
                                       <%=(count / 2) == 1 ? "" : "style='background:#dddddd'"%>  
                                       BACKGROUND="#dddddd" id="<%=e.getProduct().getId()%>"   name="<%=e.getProduct().getId()%>" min="1" max="5" step="1" value="<%=e.getQuantity()%>"/>  
                            </dd>
                    </td>
                    <td> <p><%=e.getProduct().getPrice()%> </td>
                    <td><p id="amount<%=count%>"><%=e.getQuantity() * e.getProduct().getPrice()%>
                        </p>
                        <script>
                          
                            var x = myFunction(<%=e.getProduct().getPrice()%>, document.getElementById("<%=e.getProduct().getId()%>").innerHTML, amount);
                            function myFunction(a, b, c) {
                                document.getElementById(c).innerHTML = a * b;
                                document.getElementById("tota").innerHTML = totalx();
                            }


                        </script></td>

                </tr>

                <% count++;
                    }
                %>
                <%
                     if(order!=null){%> 
                     <tr>
                    <td colspan="3"><p >Total</p></td>
                    <td colspan="1" id="tota"><p><%=order.getAmount()%></p></td>
                </tr>
                <%
                linex=order.getLines().size();
            }%>
                
            </table>

            <%} else { %>
            <h2>YOU BY NOTHING</h2>
            <%}%>
        </td>   


    </tr>
    <!-- /.col-lg-9 -->

</tr>   
<td>
    <input placeholder="Your name" name="customername" type="text" tabindex="1" required autofocus 
           <% if (customer.getName() != null) {
                   out.print("value=" + customer.getName() + " ");
               }%> >

    <input placeholder="Your Address" name="address" type="text" tabindex="1" 
           <% if (customer.getAddress() != null) {
                   out.print("value=" + customer.getAddress() + " ");
               }%> required>

    <input placeholder="Your Phone Number" name="numberphone" type="tel" tabindex="3" 
           <% if (customer.getNumberphone() != null) {
                   out.print("value=" + customer.getNumberphone() + " ");
               }%> required>



</td>   
</table>   

<button name="submit" class="btn btn-lg btn-primary text-uppercase move-left" type="submit" onclick="submitForms()" id="contact-submit" data-submit="...Sending">Submit</button>

</form>

<!-- /.col-lg-3 -->
<script type="text/javascript">
    function submitForms() {

        document.forms("contact").submit();
    }
    function totalx() {
          var xz = 0;console.log(<%=count%>);
        for (i = 1; i <= <%=linex%> ; i++) {
            xz += parseFloat(document.getElementById("amount" + i).innerHTML);
        }
      
            return xz;
    }
</script>
<script>
    function getsub() {
        $(document).ready(function () {
            $("#orderline").submit();
            $("#contact").submit();
        });
    }
</script>



<!-- Bootstrap core JavaScript -->


</body>

</html>