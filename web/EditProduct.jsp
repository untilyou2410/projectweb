<%-- 
    Document   : productView
    Created on : Oct 31, 2019, 9:17:40 PM
    Author     : anhnb
--%>

<%@page import="modal.Category"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="modal.Account"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <title>Shop Homepage - Start Bootstrap Template</title>

        <!-- Bootstrap core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="css/shop-homepage.css" rel="stylesheet">
        <%
            ArrayList<Category> listCate = (ArrayList<Category>) request.getAttribute("categories");
        %>
    </head>
    <body>

        <!-- Navigation -->
        <%@include file="header.jsp" %>
        <br></br>


        <div class="container">

            <hr>
            <div class="card">
                <div class="row">
                    <aside class="col-sm-5 border-right">
                        <article class="gallery-wrap"> 
                            <div class="img-big-wrap">
                                <div > <a href="#"><img style="max-width: -webkit-fill-available;" src="image/${requestScope.product.getImgPath()}"></a></div>
                                <!-- slider-product.// -->
                                <!-- slider-nav.// -->
                        </article> <!-- gallery-wrap .end// -->
                    </aside>

                    <!-- sss-->        
                    <aside class="col-sm-7">
                        <article class="card-body p-5">
                            <h3 class="title mb-3">${requestScope.category.getName()} ${requestScope.product.getName()} </h3>



                            <!-- Page Content -->
                            <div class="container">
                                <form action="Edit" method="post" id="frm" enctype='multipart/form-data'>
                                    <table style="width:100%">
                                        <input type="hidden"  name="id" value = "${requestScope.product.getId()}">
                                        <tr>
                                            <td>Name</td>
                                            <td><input type="text" name="prname" value = "${requestScope.product.getName()}"> </td>
                                        </tr>
                                        <tr>
                                            <td>Category Name</td>
                                            <td><select id="cars" name="cate" style="width: 185px">
                                                    <%for (Category e : listCate) {
                                                    %>
                                                    <option value="<%=e.getId()%>"><%=e.getName()%></option>
                                                    <%
                                                        }
                                                    %>


                                                </select></td>
                                        </tr>
                                        <tr><td>Price</td><td><input type="number" name="price" value="${requestScope.product.getPrice()}"></td></tr>
                                        <tr>
                                            <td>Size M: </td>
                                            <td><input type="number" name="m" value=${requestScope.product.getM()} ></td>
                                        </tr>
                                        <tr>
                                            <td>Size XL: </td>  <td><input type="number" name="xl" value =${requestScope.product.getXL()}></td>
                                        </tr>
                                        <tr>
                                            <td>Size XXL: </td>  <td><input type="number" name="xxl" value = ${requestScope.product.getXXL()}></td>
                                        </tr>
                                        <tr><td>Description</td> 
                                            <td> 
                                                <textarea  name="description" rows="4" cols="50">${requestScope.product.getDescription()}</textarea>
                                            </td></tr>
                                        <tr>
                                            <td>Is Active: </td>  <td>
                                                <input type="radio" id="male" name="active" checked value="1" >
                                                <label for="male">Active</label><br>
                                                <input type="radio" id="female" name="active" value="0">
                                                <label for="female">Deadctive</label><br></td>
                                        </tr>
                                    </table>


                                </form>

                            </div>

                            <hr>


                            <input type="button" onclick="document.getElementById('frm').submit();" class="btn btn-lg btn-outline-primary text-uppercase" value="submit"> <i class="fas fa-shopping-cart" value="Add to cart"></i>  </input>
                        </article> <!-- card-body.// -->
                    </aside> <!-- col.// -->
                </div> <!-- row.// -->
            </div> <!-- card.// -->


        </div>
        <!--container.//-->


        <br><br><br>
        <article class="bg-secondary mb-3">  
            <div class="card-body text-center">
                <h4 class="text-white">HTML UI KIT <br> Ready to use Bootstrap 4 components and templates </h4>
            </div>
            <br><br><br>
        </article>
    </body>
</html>
