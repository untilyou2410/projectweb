<%-- 
    Document   : UploadFile
    Created on : Nov 14, 2020, 6:41:43 PM
    Author     : anhnb
--%>


<%@page import="modal.Category"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <title>Shop Homepage - Start Bootstrap Template</title>

        <!-- Bootstrap core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="css/shop-homepage.css" rel="stylesheet">
        <%
            ArrayList<Category> listCate = (ArrayList<Category>) request.getAttribute("categories");
        %>
    </head>
    <body>
        <%@include file="header.jsp" %>
        <br></br>
        <br></br>


        <!-- Page Content -->
        <div class="container">
            <form action="UploadFil" method="post" enctype='multipart/form-data'>
                <table style="width:100%">

                    <tr>
                        <td>Name</td>
                        <td><input type="text" name="prname"></td>
                    </tr>
                    <tr>
                        <td>Category Name</td>
                        <td><select id="cars" name="cate" style="width: 185px">
                                <%for (Category e : listCate) {
                                %>
                                <option value="<%=e.getId()%>"><%=e.getName()%></option>
                                <%
                                    }
                                %>


                            </select></td>
                    </tr>
                    <tr>
                        <td>Size M: </td>
                        <td><input type="number" name="m" min="0" value="0"></td>
                    </tr>
                    <tr>
                        <td>Size XL: </td>  <td><input type="number" name="xl" min="0" value="0"></td>
                    </tr>
                    <tr>
                        <td>Size XXL: </td>  <td><input type="number" name="xxl" min="0" value="0"></td>
                    </tr>
                    <tr>

                        <td> Description: </td>  <td> <input type="text" name="description" /></td>
                    </tr>
                    <tr>

                        <td> Image: </td>  <td> <input type="file" name="file"  required/></td>

                    </tr>
                    <tr>

                        <td>  </td>  <td>  <input type="submit" /></td>

                    </tr>
                </table>




            </form>

        </div>
    </body>
</html>
